package kp1_try;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import exam.exam1;

@RunWith(value = Parameterized.class)
public class examTest {

	exam1 ex;
	int a;
	boolean result;

	public examTest(int a, boolean result) {
		this.a = a;
		this.result = result;
	}

	@Parameters
	public static Collection<Object[]> createParameters() {
		return Arrays.asList(new Object[][] { { 0, false }, { 1, false }, { 5, true }, { 4, false } });
	}

	@Test
	public void testExamFunction() {
		assertEquals(result, ex.exam(a));
		;
	}

}
